-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 02 Feb 2023 pada 18.55
-- Versi server: 5.7.33
-- Versi PHP: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `humas-poltek`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_gambar_kegiatan`
--

CREATE TABLE `tb_gambar_kegiatan` (
  `id_gambar_kegiatan` int(11) NOT NULL,
  `id_repo` int(11) DEFAULT NULL,
  `nama_file` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_gambar_kegiatan`
--

INSERT INTO `tb_gambar_kegiatan` (`id_gambar_kegiatan`, `id_repo`, `nama_file`) VALUES
(1, 1, '18ad52dd743a76ef87b1a144639e677e.jpg'),
(2, 1, '34ede12aafdcfa07a89ed544295e8e86.jpeg'),
(3, 2, '778e39e576353daa6391306227d18bcb.JPG'),
(4, 3, 'ec9bdfedb47ef66ab17c99f11761208d.JPG'),
(5, 4, 'ffd49ec2fdf6565d7904656225472ce3.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jabatan`
--

CREATE TABLE `tb_jabatan` (
  `id_jabatan` int(5) NOT NULL,
  `nama_jabatan` varchar(50) DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_jabatan`
--

INSERT INTO `tb_jabatan` (`id_jabatan`, `nama_jabatan`, `keterangan`) VALUES
(1, 'WD3', 'Wakir Direktur 3'),
(2, 'staff humas', 'staff');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jenis_surat`
--

CREATE TABLE `tb_jenis_surat` (
  `id_jenis_surat` int(5) NOT NULL,
  `nama_jenis` varchar(50) DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_jenis_surat`
--

INSERT INTO `tb_jenis_surat` (`id_jenis_surat`, `nama_jenis`, `keterangan`) VALUES
(1, 'SMK', NULL),
(2, 'Perguruan Tinggi', NULL),
(3, 'Perusahaan', NULL),
(4, 'Instansi Pemerintah', NULL),
(5, 'Lembaga Sosial', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kegiatan`
--

CREATE TABLE `tb_kegiatan` (
  `id_kegiatan` int(5) NOT NULL,
  `nama_kegiatan` varchar(75) NOT NULL,
  `aktif` enum('Y','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_kegiatan`
--

INSERT INTO `tb_kegiatan` (`id_kegiatan`, `nama_kegiatan`, `aktif`) VALUES
(1, 'pemagangan', 'Y'),
(2, 'seminar edukasi', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_repository`
--

CREATE TABLE `tb_repository` (
  `id_repo` int(5) NOT NULL,
  `id_jenis_surat` int(5) DEFAULT NULL,
  `type_repo` int(5) DEFAULT NULL COMMENT '1 : MoU 2 : MoA',
  `judul_surat` varchar(50) DEFAULT NULL,
  `mitra` varchar(50) DEFAULT NULL,
  `no_pihak_pertama` varchar(50) DEFAULT NULL,
  `no_pihak_kedua` varchar(50) DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_berakhir` date DEFAULT NULL,
  `kegiatan_perjanjian` varchar(128) DEFAULT NULL,
  `file_surat` varchar(50) DEFAULT NULL,
  `user_created` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `is_delete` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_repository`
--

INSERT INTO `tb_repository` (`id_repo`, `id_jenis_surat`, `type_repo`, `judul_surat`, `mitra`, `no_pihak_pertama`, `no_pihak_kedua`, `tgl_mulai`, `tgl_berakhir`, `kegiatan_perjanjian`, `file_surat`, `user_created`, `created_at`, `is_delete`) VALUES
(1, 1, 2, 'testing 1', 'smk migas', 'pLvzoy5fNv', 'HFw5DusaLu', '2022-01-10', '2023-02-23', 'r3Jlh9omzW', 'sample.doc', 'admin', '2023-01-09 23:40:07', 'Y'),
(2, 3, 1, 'RVJmr3P0nh', 'polbeng', 'RsrVfMfvUt', '12vlXfkC6E', '2023-01-09', '2023-01-28', 'R6NKGbtg3d', 'FILE-1-1673213205.docx', 'admin', '2023-01-08 21:26:45', 'N'),
(3, 2, 1, 'rersefasd', 'smk migas', 'sdf', 'sdf', '2023-01-18', '2023-01-26', '<ul><li>GlGACNWDHH</li><li>tesaaaaa</li></ul>', 'FILE-MoA-1673308054.docx', 'admin', '2023-01-30 12:18:41', 'N'),
(4, 3, 1, 'tessss', 'easd', '345345ws', '345ew3', '2023-02-02', '2024-02-02', NULL, 'FILE-MoA-1675356493.pdf', 'admin', '2023-02-02 18:54:03', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_repository_kegiatan`
--

CREATE TABLE `tb_repository_kegiatan` (
  `id_repo_kegiatan` int(5) NOT NULL,
  `id_repo` int(5) NOT NULL,
  `id_kegiatan` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_repository_kegiatan`
--

INSERT INTO `tb_repository_kegiatan` (`id_repo_kegiatan`, `id_repo`, `id_kegiatan`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(4, 2, 2),
(5, 3, 1),
(7, 4, 2),
(9, 4, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_role`
--

CREATE TABLE `tb_role` (
  `id_role` int(11) NOT NULL,
  `nama_role` varchar(50) DEFAULT NULL,
  `keteragan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_role`
--

INSERT INTO `tb_role` (`id_role`, `nama_role`, `keteragan`) VALUES
(1, 'Administrator', NULL),
(2, 'User', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(5) NOT NULL,
  `username` varchar(25) DEFAULT NULL,
  `password` varchar(75) DEFAULT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `id_role` int(5) DEFAULT NULL,
  `nrp` varchar(50) DEFAULT NULL,
  `id_jabatan` int(10) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `password`, `firstname`, `lastname`, `id_role`, `nrp`, `id_jabatan`, `created_at`) VALUES
(1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'admin', 'strator', 1, '12345678', 2, '2023-01-15 21:35:22'),
(3, 'staff', 'e10adc3949ba59abbe56e057f20f883e', 'Staff', 'Humas', 2, '123456789', 2, '2023-01-15 22:03:59');

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_user`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_user` (
`id_user` int(5)
,`nrp` varchar(50)
,`username` varchar(25)
,`password` varchar(75)
,`nama` varchar(91)
,`id_role` int(11)
,`nama_role` varchar(50)
,`nama_jabatan` varchar(50)
,`created_at` datetime
);

-- --------------------------------------------------------

--
-- Struktur untuk view `v_user`
--
DROP TABLE IF EXISTS `v_user`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_user`  AS SELECT `a`.`id_user` AS `id_user`, `a`.`nrp` AS `nrp`, `a`.`username` AS `username`, `a`.`password` AS `password`, concat(`a`.`firstname`,' ',`a`.`lastname`) AS `nama`, `b`.`id_role` AS `id_role`, `b`.`nama_role` AS `nama_role`, `c`.`nama_jabatan` AS `nama_jabatan`, `a`.`created_at` AS `created_at` FROM ((`tb_user` `a` join `tb_role` `b` on((`a`.`id_role` = `b`.`id_role`))) join `tb_jabatan` `c` on((`a`.`id_jabatan` = `c`.`id_jabatan`))) ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_gambar_kegiatan`
--
ALTER TABLE `tb_gambar_kegiatan`
  ADD PRIMARY KEY (`id_gambar_kegiatan`);

--
-- Indeks untuk tabel `tb_jabatan`
--
ALTER TABLE `tb_jabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indeks untuk tabel `tb_jenis_surat`
--
ALTER TABLE `tb_jenis_surat`
  ADD PRIMARY KEY (`id_jenis_surat`);

--
-- Indeks untuk tabel `tb_kegiatan`
--
ALTER TABLE `tb_kegiatan`
  ADD PRIMARY KEY (`id_kegiatan`);

--
-- Indeks untuk tabel `tb_repository`
--
ALTER TABLE `tb_repository`
  ADD PRIMARY KEY (`id_repo`),
  ADD KEY `id_ jenis_surat` (`id_jenis_surat`) USING BTREE;

--
-- Indeks untuk tabel `tb_repository_kegiatan`
--
ALTER TABLE `tb_repository_kegiatan`
  ADD PRIMARY KEY (`id_repo_kegiatan`);

--
-- Indeks untuk tabel `tb_role`
--
ALTER TABLE `tb_role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_gambar_kegiatan`
--
ALTER TABLE `tb_gambar_kegiatan`
  MODIFY `id_gambar_kegiatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tb_jabatan`
--
ALTER TABLE `tb_jabatan`
  MODIFY `id_jabatan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tb_jenis_surat`
--
ALTER TABLE `tb_jenis_surat`
  MODIFY `id_jenis_surat` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tb_kegiatan`
--
ALTER TABLE `tb_kegiatan`
  MODIFY `id_kegiatan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tb_repository`
--
ALTER TABLE `tb_repository`
  MODIFY `id_repo` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tb_repository_kegiatan`
--
ALTER TABLE `tb_repository_kegiatan`
  MODIFY `id_repo_kegiatan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tb_role`
--
ALTER TABLE `tb_role`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
