<?php 

use Jenssegers\Blade\Blade;
if (!function_exists('view')) {

    /**
     * If the method of view.
     * 
     *  @return bool  */
    function view($view, $data = []) {
        $path = APPPATH.'views';
        $blade = new Blade($path, $path . '/cache');

        echo $blade->make($view, $data);
    }
}

if(!function_exists('fun_route')){
	function fun_route($data=[])
	{
		$split = explode('\\',$data[0]);

		return $split[1];
	}
}

if(!function_exists('json_output')){

	function json_output($data)
	{
		$CI =& get_instance();
		return $CI->output->set_content_type('application/json')->set_output(json_encode($data));
	}
}

if(!function_exists('userdata')){

	function userdata($type)
	{
		$CI =& get_instance();
		return $CI->session->userdata($type);
	}
}

if(!function_exists('flashdata')){

	function flashdata($type,$set=[])
	{
		$CI =& get_instance();
		if($set){
			return $CI->session->set_flashdata($type, $set[0]);
			
		}else{
			return $CI->session->flashdata($type);
		}
		
	}
}

if(!function_exists('FormBuilder')){

	function FormBuilder($setting)
	{
		$CI =& get_instance();
		$CI->load->library('form');
		return $CI->form->template($setting);
		
		
	}
}



/* End of file view_helper.php and path \application\helpers\view_helper.php */
