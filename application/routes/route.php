<?php 

Route::filter('logged_in', function(){
	
	if($this->auth->is_logged() == 'FALSE')
	{	
		flashdata('error',['session habis']);
		redirect(base_url());
	}

});

Route::any('login', 'login', array(), function(){
	Route::post('check-user', 'login/CheckingAccount',array());
	Route::get('is-logout', 'login/IsLogout');
});

Route::get('dashboard', 'dashboard', array('before' => 'logged_in'));
Route::get('dashboard/widget-app', 'dashboard/datawidgetapp', array('before' => 'logged_in'));
Route::get('data-kerjasama', 'data', array('before' => 'logged_in'));
Route::match(array('GET', 'POST'),'data-kerjasama/tables', 'data/DataTables', array('before' => 'logged_in'));
Route::post('data-kerjasama/save', 'data/Save', array('before' => 'logged_in'));
Route::get('report-kerjasama', 'data/index/laporan', array('before' => 'logged_in'));
Route::post('data-kerjasama/delete', 'data/Delete', array('before' => 'logged_in'));
Route::get('data-jenis', 'data/SelectJenisSurat', array('before' => 'logged_in'));
Route::get('data-image', 'data/ListImageKerjasma', array('before' => 'logged_in'));

Route::any('data-kegiatan','kegiatan/index',array(),function(){
	Route::post('save', 'kegiatan/save',array('before' => 'logged_in'));
	Route::post('delete', 'kegiatan/delete',array('before' => 'logged_in'));
	Route::match(['GET','POST'],'tables', 'kegiatan/datalist', array('before' => 'logged_in'));
});
Route::get('list-kegiatan', 'kegiatan/data', array('before' => 'logged_in'));


Route::any('data-pengguna', 'users', array('before' => 'logged_in'), function(){
	Route::post('save', 'users/savepengguna',array('before' => 'logged_in'));
	Route::post('delete', 'users/deletepengguna',array('before' => 'logged_in'));
	Route::match(['GET','POST'],'tables-pengguna', 'users/datapengguna',array('before' => 'logged_in'));
	Route::match(['GET','POST'],'list-role', 'users/datarole',array('before' => 'logged_in'));
	Route::match(['GET','POST'],'list-jabatan', 'users/datajabatan',array('before' => 'logged_in'));
});


?>
