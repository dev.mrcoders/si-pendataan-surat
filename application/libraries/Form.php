<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Form
{
	public function __construct()
	{
		$this->CI = &get_instance();
	}

	public function _input($attribut, $i = null)
	{
		switch ($attribut['type']) {
			case 'text':
				$html = '<input type="text" class="form-control ' . $attribut['class'] . '" name="' . $attribut['name'] . '" id="' . $attribut['id'] . '" >';
				break;
			case 'password':
				$html = '<input type="password" class="form-control ' . $attribut['class'] . '" name="' . $attribut['name'] . '" id="' . $attribut['id'] . '" >';
				break;
			case 'checkbox':
				$html = '<input type="checkbox" class="form-control ' . $attribut['class'] . '" name="' . $attribut['name'] . '" id="' . $attribut['id'] . '"  >';
				break;
			case 'radio':
				$html = '<input type="radio" class="custom-control-input" id="' . $attribut['id'] . '_' . $i . '" name="' . $attribut['name'] . '" value="' . $i . '">';
				break;
			case 'select':
				$html = '<select class="form-control ' . $attribut['class'] . '" name="' . $attribut['name'] . '" id="' . $attribut['id'] . '" ></select>';
				break;
			case 'file':
				$html = '<input type="file" class="form-control ' . $attribut['class'] . '" name="' . $attribut['name'] . '" id="' . $attribut['id'] . '" accept="' . $attribut['accept'] . '" >';
				break;
		}

		return $html;
	}

	public function _label($label)
	{
		$html = '<label for="inputEmail3" class="col-3 text-right control-label col-form-label">' . $label . '</label>';
		return $html;
	}


	public function template($attribut)
	{
		$html = '';
		$html = $html . '<div class="form-group row align-items-center m-b-0">';
		$html = $html . $this->_label($attribut['label']);
		$html = $html . '<div class="col-9 border-left p-b-10 p-t-10">';
		if ($attribut['type'] == 'radio') {
			foreach ($attribut['vlabel'] as $i => $vl) {
				$html = $html . '<div class="custom-control custom-radio">';
				$html = $html . $this->_input($attribut, $i);
				$html = $html . '<label class="custom-control-label" for="' . $attribut['id'] . '_' . $i . '">' . $vl . '</label></div>';
			}
		} else {
			$html = $html . $this->_input($attribut);
			// $html = $html.'<div class="invalid-feedback txt-'.$attribut['class'].'"></div>';
		}

		$html = $html . '</div></div>';

		echo $html;
	}

	public function UploadDocument($data)
	{

		$config['upload_path'] = './uploads/document/';
		$config['allowed_types'] = 'pdf|docx|doc|jpeg|jpg';
		$config['file_name'] = 'FILE-' . $data['type_surat'] . '-' . time();

		$this->CI->load->library('upload');
		$this->CI->upload->initialize($config);
		if (!empty($_FILES[$data['files']]['name'])) {
			if (!$this->CI->upload->do_upload($data['files'])) {
				$data = ['status' => 404, 'success' => false, 'res' => $this->CI->upload->display_errors() . 'aaaa'];
			} else {
				$img = $this->CI->upload->data();
				$data = ['status' => 200, 'success' => true, 'res' => $img['file_name']];
			}
		} else {
			$data = ['status' => 200, 'success' => false, 'res' => ''];
		}
		return $data;
	}

	public function UploadImages($data)
	{

		$config['upload_path'] = './uploads/document/';
		$config['allowed_types'] = 'jpeg|jpg';
		$config['remove_spaces'] = TRUE;
		$config['encrypt_name'] = TRUE;

		$this->CI->load->library('upload', $config);
		if (!empty($_FILES[$data['files']]['name'])) {
			if (!$this->CI->upload->do_upload($data['files'])) {
				$data = ['status' => 404, 'success' => false, 'res' => $this->CI->upload->display_errors() . 'aaaa'];
			} else {
				$img = $this->CI->upload->data();
				$data = ['status' => 200, 'success' => true, 'res' => $img['file_name']];
			}
		} else {
			$data = ['status' => 200, 'success' => false, 'res' => ''];
		}


		return $data;
	}

	public function UploadImagesmany()
	{
		$countfiles = count($_FILES['images']['name']);
		// $DataImage=[];
		for ($i = 0; $i < $countfiles; $i++) {
			if (!empty($_FILES['images']['name'][$i])) {
				$_FILES['file']['name'] = $_FILES['images']['name'][$i];
				$_FILES['file']['type'] = $_FILES['images']['type'][$i];
				$_FILES['file']['tmp_name'] = $_FILES['images']['tmp_name'][$i];
				$_FILES['file']['error'] = $_FILES['images']['error'][$i];
				$_FILES['file']['size'] = $_FILES['images']['size'][$i];

				$setup['upload_path'] = './uploads/images/';
				$setup['allowed_types'] = 'jpeg|jpg|png';
				$setup['remove_spaces'] = TRUE;
				$setup['encrypt_name'] = TRUE;

				$this->CI->load->library('upload', $setup);
				$this->CI->upload->initialize($setup);
				if ($this->CI->upload->do_upload('file')) {
					$Img = $this->CI->upload->data();
					$FileName = $Img['file_name'];
					$DataImage[] = ['filename' => $FileName];
					$res = ['status' => true, 'res' => $DataImage];
				} else {
					$res = ['status' => false, 'res' => $this->CI->upload->display_errors() . 'aaa'];
				}
			}
		}

		return $res;
	}
}


/* End of file Form.php and path \application\libraries\Form.php */
