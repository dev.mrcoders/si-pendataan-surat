<?php defined('BASEPATH') or exit('No direct script access allowed');

class Users extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		return view('halaman.pengguna');
	}

	public function DataPengguna()
	{
		if ($this->input->get('id')) {
			$this->db->where('id_user', $this->input->get('id'));
			$list = $this->db->get('tb_user')->row();
		} else {
			$this->db->where('id_role !=', 1);
			
			$list = $this->db->get('v_user')->result();
		}
		json_output($list);
	}

	public function DataRole()
	{
		$list = $this->db->get('tb_role')->result();
		json_output($list);
	}
	public function Datajabatan()
	{
		$list = $this->db->get('tb_jabatan')->result();
		json_output($list);
	}

	public function SavePengguna()
	{

		if ($this->input->post('id_')) {
			$DataForm = [
				'username' => $this->input->post('username'),
				'firstname' => $this->input->post('firstname'),
				'lastname' => $this->input->post('lastname'),
				'id_role' => $this->input->post('role'),
				'id_jabatan' => $this->input->post('jabatan'),
				'nrp' => $this->input->post('nrp'),
				'created_at' => date('Y-m-d H:i:s'),
			];
			if ($this->input->post('password')) {
				$DataForm['password'] = md5($this->input->post('password'));
			}

			$this->db->where('id_user', $this->input->post('id_'));
			$hasil = $this->db->update('tb_user', $DataForm);
			
		} else {
			$DataForm = [
				'username' => $this->input->post('username'),
				'password'=>md5($this->input->post('password')),
				'firstname' => $this->input->post('firstname'),
				'lastname' => $this->input->post('lastname'),
				'id_role' => $this->input->post('role'),
				'id_jabatan' => $this->input->post('jabatan'),
				'nrp' => $this->input->post('nrp'),
				'created_at' => date('Y-m-d H:i:s'),
			];

			$hasil = $this->db->insert('tb_user', $DataForm);
		}
		

		json_output($hasil);
	}

	public function DeletePengguna()
	{
		$this->db->where('id_user', $this->input->post('id'));
		$hasil = $this->db->delete('tb_user');

		json_output($hasil);
	}
}

/* End of file Users.php and path \application\controllers\Users.php */
