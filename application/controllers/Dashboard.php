<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

		return view('halaman.dashboard');
	}

	public function DataWidgetApp()
	{
		$JmlSuratKerjasama = $this->db->select('count(id_repo)as jml')->from('tb_repository')->get()->row()->jml;
		$JmlMitraKerjasama = $this->db->select('mitra')->from('tb_repository')->group_by('mitra')->get()->result();
		$JmlKerjasamaBerakhir = $this->db->select('count(id_repo)as jml')->from('tb_repository')->where('tgl_berakhir <',date('Y-m-d H:i'))->get()->row()->jml;
		$JmlKerjasamaAktif = $this->db->select('count(id_repo)as jml')->from('tb_repository')->where('tgl_berakhir >',date('Y-m-d H:i'))->get()->row()->jml;

		$JmlByJenis = $this->db->select('a.nama_jenis,(SELECT count(b.id_repo) FROM tb_repository b WHERE b.id_jenis_surat=a.id_jenis_surat)as jml')
						->from('tb_jenis_surat a')
						->get()->result();
		
		$Response=[
			'success'=>true,
			'data'=>[
				'jmlkerjasama'=>intval($JmlSuratKerjasama) ,
				'jmlmitra'=>count($JmlMitraKerjasama),
				'jmlberakhir'=>intval($JmlKerjasamaBerakhir),
				'jmlaktif'=>intval($JmlKerjasamaAktif),
				'byjenis'=>$JmlByJenis
			]
		];

		$this->output->set_content_type('application/json')->set_output(json_encode($Response));
		
		
		
	}
}

/* End of file Dashboard.php and path \application\controllers\Dashboard.php */
