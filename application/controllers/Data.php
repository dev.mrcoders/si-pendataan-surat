<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Data extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Repository_model', 'repo');
	}

	public function index($page = null)
	{
		if ($page != null) {
			return view('halaman.laporan');
		} else {
			return view('halaman.kerjasama');
		}
	}

	public function DataTables()
	{
		$list = $this->repo->_view_data();
		json_output($list);
	}

	public function Save()
	{

		$type_surat = ($this->input->post('type_surat') == 1? 'MoA':'MoU');
		$SaveDocument = $this->form->UploadDocument(['files' => 'file_surat', 'type_surat' => $type_surat]);
		if ($SaveDocument['status'] == 200) {
			if ($SaveDocument['success']) {
				$FormData = [
					'id_jenis_surat' => $this->input->post('jenis_surat'),
					'type_repo' => $this->input->post('type_surat'),
					'judul_surat' => $this->input->post('judul_surat'),
					'mitra' => $this->input->post('mitra'),
					'no_pihak_pertama' => $this->input->post('no_pihak_pertama'),
					'no_pihak_kedua' => $this->input->post('no_pihak_kedua'),
					'tgl_mulai' => $this->input->post('tgl_mulai'),
					'tgl_berakhir' => $this->input->post('tgl_berakhir'),
					'file_surat' => $SaveDocument['res'],
					'user_created' => $this->session->userdata('username'),
					'created_at' => date('Y-m-d H:i:s')
				];
			} else {
				$FormData = [
					'id_jenis_surat' => $this->input->post('jenis_surat'),
					'type_repo' => $this->input->post('type_surat'),
					'judul_surat' => $this->input->post('judul_surat'),
					'mitra' => $this->input->post('mitra'),
					'no_pihak_pertama' => $this->input->post('no_pihak_pertama'),
					'no_pihak_kedua' => $this->input->post('no_pihak_kedua'),
					'tgl_mulai' => $this->input->post('tgl_mulai'),
					'tgl_berakhir' => $this->input->post('tgl_berakhir'),
					'user_created' => $this->session->userdata('username'),
					'created_at' => date('Y-m-d H:i:s')
				];
			}
			if ($this->input->post('id_') == 0) {
				$this->db->insert('tb_repository', $FormData);
				$IdRepo = $this->db->insert_id();
				$DataImage = [];
				$ImagesUploads = $this->form->UploadImagesmany();
				foreach($this->input->post('kegiatan_perjanjian') as $kgt){
					$DataKgp []=[
						'id_repo'=>$IdRepo,
						'id_kegiatan'=>$kgt
					];
				}

				foreach ($ImagesUploads['res'] as $i => $file_name) {
					$Image = [
						'id_repo' => $IdRepo,
						'nama_file' => $file_name['filename']
					];
					$DataImage[] = $Image;
				}

				$this->db->insert_batch('tb_repository_kegiatan', $DataKgp);
				$this->db->insert_batch('tb_gambar_kegiatan', $DataImage);
				$Response = [
					'success' => true,
					'message' => "Data Berhasil Di Simpan",
					'data' => []
				];
			} else {
				$GetDatalist = $this->db->get_where('tb_repository_kegiatan', ['id_repo'=>$this->input->post('id_')])->result();
				$ListData=[];
				foreach($GetDatalist as $rows){
					$ListData[]=$rows->id_kegiatan;
				}

				// insert new list
				foreach($this->input->post('kegiatan_perjanjian') as $inputs){
					if(!in_array($inputs,$ListData)){						
						$this->db->insert('tb_repository_kegiatan', ['id_repo'=>$this->input->post('id_'),'id_kegiatan'=>$inputs]);
					}
				}

				foreach($ListData as $listes){
					if(!in_array($listes,$this->input->post('kegiatan_perjanjian'))){
						$this->db->where('id_repo', $this->input->post('id_'));
						$this->db->where('id_kegiatan', $listes);
						$this->db->delete('tb_repository_kegiatan');
						
					}
				}
				
				$this->db->where('id_repo', $this->input->post('id_'));
				$this->db->update('tb_repository', $FormData);
				$Response = [
					'success' => true,
					'message' => "Data Berhasil Di Ubah",
					'data' => []
				];
			}
		} else {
			$Response = [
				'success' => false,
				'message' => 'Data Gagal Di simpan',
				'data'=>[
					'id'=>$this->input->post('id_'),
					'images'=>$SaveDocument
				]
			];
		}

		json_output($Response);
	}


	public function SelectJenisSurat()
	{
		$Data = $this->db->get('tb_jenis_surat')->result();
		foreach ($Data as $key => $value) {
			$Res[] = [
				'id' => $value->id_jenis_surat,
				'text' => $value->nama_jenis
			];
		}
		$NewRes = [
			'id' => '',
			'text' => '== Pilih data =='
		];
		array_unshift($Res, $NewRes);

		json_output($Res);
	}

	public function ListImageKerjasma()
	{
		$List = $this->repo->GetListImage($this->input->get('id'));

		json_output($List);
	}

	public function Delete()
	{
		$hasil = $this->repo->Deleted($this->input->post('id'));
		json_output($hasil);
	}

}

/* End of file Data.php and path \application\controllers\Data.php */
