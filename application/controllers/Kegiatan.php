<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Kegiatan extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
		return view('halaman.kegiatan');
    }

	public function Data()
	{
		$this->db->join('tb_kegiatan b', 'a.id_kegiatan=b.id_kegiatan', 'inne');
		$this->db->where('id_repo', $this->input->get('id'));
		$list = $this->db->get('tb_repository_kegiatan a');

		$this->output->set_content_type('application/json')->set_output(json_encode($list->result()));
		
	}

	public function DataList()
	{
		if($this->input->get('id')){
			$this->db->where('id_kegiatan', $this->input->get('id'));
		}
		if($this->input->get('search')){
			$this->db->like('nama_kegiatan', $this->input->get('search'), 'BOTH');
		}

		$list = $this->db->get('tb_kegiatan ');
		$result = ($this->input->get('id')? $list->row():$list->result());
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function Save()
	{
		$DataPost = [
			'nama_kegiatan'=>$this->input->post('nama_kegiatan'),
			'aktif'=>$this->input->post('aktif')
		];

		if($this->input->post('id_') ==''){
			$this->db->insert('tb_kegiatan', $DataPost);
			
			$Response = [
				'success' => true,
				'message' => "Data Berhasil Di Simpan",
				'data' => []
			];
		}else{
			$this->db->where('id_kegiatan', $this->input->post('id_'));
			$this->db->update('tb_kegiatan', $DataPost);
			
			$Response = [
				'success' => true,
				'message' => "Data Berhasil Di Ubah",
				'data' => []
			];
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($Response));
		
	}

	public function Delete()
	{
		$id = $this->input->post('id');

		$ChekRelasi = $this->db->get_where('tb_repository_kegiatan', ['id_kegiatan'=>$id]);
		if($ChekRelasi->num_rows() > 0){
			$Response = [
				'success' => false,
				'message' => "Jenis Tidak Bisa Di Hapus, Jenis sedang digunakan",
				'data' => []
			];
		}else{
			$this->db->where('id_kegiatan', $id);
			$this->db->delete('tb_kegiatan');
			
			$Response = [
				'success' => true,
				'message' => "Jenis data berhasil di hapus",
				'data' => []
			];
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($Response));
		
		
	}
}

/* End of file Kegiatan.php and path \application\controllers\Kegiatan.php */
