<?php
defined('BASEPATH') or exit('No direct script access allowed');

use controllers\welcome;


class Login extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model', 'user');
	}

	public function index()
	{
		$data = [
			'test' => 'aa'
		];
		return view('login', $data);
	}

	public function CheckingAccount()
	{
		$stats = false;
		$rules = [
			[
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'required'
			],
			[
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required'
			]
		];
		$this->form_validation->set_rules($rules);
		$this->form_validation->set_message('required', '{field} Tidak Boleh Kosong');
		if ($this->form_validation->run() == FALSE) {
			$response = [
				'status' => 404,
				'message' => validation_errors()
			];
			json_output($response);
		} else {
			$FormPost = [
				'username' => $this->input->post('username'),
				'password' => md5($this->input->post('password'))
			];
			$LoginUser = $this->user->Is_Loggin($FormPost);
			if ($LoginUser) {
				$sess_user=[
					'username'=>$LoginUser->username,
					'nrp'=>$LoginUser->nrp,
					'role'=>$LoginUser->id_role,
					'login'=>TRUE
				];
				
				$this->session->set_userdata( $sess_user);
				
				$response = [
					'status' => 200,
					'message' => 'Login Berhasil',
					'redirect' => 'dashboard'
				];
			} else {
				$response = [
					'status' => 404,
					'message' => 'Login Gagal, Username Or Password Salah',
					'redirect' => ''
				];
			}
			json_output($response);
		}
	}

	public function IsLogout()
	{
		$data_session = array('username'=>"",'nrp'=>"",'role'=>"",'login'=>"");
      	$this->session->unset_userdata($data_session);//clear session
      	$this->session->sess_destroy();//tutup session
      	redirect(base_url());
      }
  }

  /* End of file Login.php and path \application\controllers\Login.php */
