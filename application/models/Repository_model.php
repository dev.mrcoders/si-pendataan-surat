<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Repository_model extends CI_Model
{
	var $table = 'tb_repository'; //nama tabel dari database
	var $table_join = 'tb_jenis_surat';
	var $column_order = array(null, 'id_jenis_surat', 'type_repo', 'judul_surat', 'mitra'); //field yang ada di table user
	var $column_search = array('id_jenis_surat', 'type_repo', 'judul_surat', 'mitra'); //field yang diizin untuk pencarian 
	var $order = array('id_repo' => 'desc'); // default order 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function _view_data()
	{
		if ($this->input->get('id')) {
			$this->db->where($this->table . '.id_repo', $this->input->get('id'));
			$this->db->where($this->table . '.is_delete', 'N');
			$this->_joined_tables();
			return $this->db->get($this->table)->row();
		} else {
			$this->_joined_tables();
			if ($this->input->post('is_delete')) {
				$this->db->where('is_delete', 'N');
			}
			if($this->input->post('jenis_surat')){
				$this->db->where('type_repo', $this->input->post('jenis_surat'));				
			}
			if($this->input->post('jenis_kerjasama')){
				$this->db->where($this->table.'.id_jenis_surat', $this->input->post('jenis_kerjasama'));				
			}
			if($this->input->post('tahun')){
				$this->db->where('SUBSTRING(tb_repository.tgl_mulai,1,4) = "'.$this->input->post('tahun').'"');
				$this->db->or_where('SUBSTRING(tb_repository.tgl_berakhir,1,4) = "'.$this->input->post('tahun').'"');
				
			}
			if($this->input->post('jenis_kegiatan')){
				$this->db->join('tb_repository_kegiatan', 'tb_repository_kegiatan.id_repo=tb_repository.id_repo', 'inner');
				$this->db->where('tb_repository_kegiatan.id_kegiatan', $this->input->post('jenis_kegiatan'));
			}
			
			$this->db->order_by('tb_repository.id_repo', 'desc');
			return $this->db->get($this->table)->result();
		}
	}

	public function _joined_tables()
	{
		$this->db->join($this->table_join, $this->table_join . '.id_jenis_surat = ' . $this->table . '.id_jenis_surat', 'inner');
	}

	public function GetListImage($id)
	{
		$this->db->join($this->table, $this->table . '.id_repo = tb_gambar_kegiatan.id_repo', 'inner');
		return $this->db->get_where('tb_gambar_kegiatan', ['tb_gambar_kegiatan.id_repo' => $id])->result();
	}

	public function Deleted($id)
	{
		$this->db->where('id_repo', $id);
		return $this->db->update($this->table, ['is_delete' => 'Y']);
	}
}


/* End of file Repository_model.php and path \application\models\Repository_model.php */
