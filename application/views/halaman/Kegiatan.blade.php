@extends('template')
@section('content')
    {{-- <style>
        tbody tr td {
            font-size: 11px;
        }
    </style> --}}
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <div class="row mb-4">
                            <div class="col-sm-6">
                                <h4 class="card-title">Data Jenis Kegiatan Perjanjian</h4>
                            </div>
                            <div class="col-sm-6 text-right">
                                <button class="btn btn-sm btn-primary tambah">Tambah Data</button>
                            </div>

                        </div>
                        <div class="table-responsive mt-3">
                            <table class="table " id="dt-table">
                                <thead class="bg-primary text-white">
                                    <tr>
                                        <th>#</th>
                                        <th>Nama kegiatan</th>
                                        <th>Aktif</th>
                                        <th>Act</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Form tambah data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" class="form-horizontal r-separator" id="form-kegiatan">
                    <input type="hidden" name="id_" value="" id="id_">
                    <div class="modal-body">
                        <div class="form-group row align-items-center m-b-0">
                            <label for="inputEmail3" class="col-3 text-right control-label col-form-label">Nama Jenis
                                Kegiatan</label>
                            <div class="col-9 border-left p-b-10 p-t-10">
                                <input type="text" name="nama_kegiatan" class="form-control" id="inputEmail3"
                                    placeholder="Nama Jenis Kegiatan" required>
                            </div>
                        </div>
                        <div class="form-group row align-items-center m-b-0">
                            <label for="inputEmail3" class="col-3 text-right control-label col-form-label">Aktif</label>
                            <div class="col-9 border-left p-b-10 p-t-10">
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input yes" id="customControlValidation2"
                                        name="aktif" required="" value="Y">
                                    <label class="custom-control-label" for="customControlValidation2">Yes</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input no" id="customControlValidation3"
                                        name="aktif" required="" value="N">
                                    <label class="custom-control-label" for="customControlValidation3">No</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $(function() {
            LoadTableDatas();

            function LoadTableDatas() {
                $(".table-responsive").css("display", "block");
                table = $("#dt-table").DataTable({
                    stateSave: true, //dapat kembali sesuai page terakhir di klik
                    destroy: true,
                    ajax: {
                        url: base_url + "data-kegiatan/tables",
                        type: "POST",

                        dataSrc: "",
                    },
                    columns: [{
                            render: function(data, type, row, meta) {
                                return meta.row + 1;
                            },
                        },
                        {
                            data: "nama_kegiatan"
                        },
                        {
                            data: "aktif",
                            render: function(data) {
                                if (data == 'Y') {
                                    return '<span class="badge badge-success">YES</span>';
                                } else {
                                    return '<span class="badge badge-danger">NO</span>';

                                }
                            }
                        },

                    ],

                    columnDefs: [{
                        targets: 3,
                        data: "id_kegiatan",
                        render: function(data, type, row, meta) {
                            return (
                                '<div class="btn-group">' +
                                '<button class="btn btn-sm btn-warning edit" data-id="' +
                                data + '">Ubah</button>' +
                                '<button class="btn btn-sm btn-danger hapus" data-id="' +
                                data + '">Hapus</button>' +
                                '</div>'
                            );
                        },
                    }, ],
                });
            }

            $('.tambah').on('click', function() {
                $('#add-modal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });

            $('#dt-table').on('click', '.edit', function() {
                let id = $(this).data('id');
                $.ajax({
                    type: "get",
                    url: base_url + "data-kegiatan/tables",
                    data: {
                        id: id
                    },
                    dataType: "json",
                    success: function(response) {
                        $('#add-modal').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('#id_').val(id);
                        $('[name="nama_kegiatan"]').val(response.nama_kegiatan);
                        if (response.aktif == 'Y') {
                            $('.yes').attr('checked', true);
                        } else {
                            $('.no').attr('checked', true);
                        }

                    }
                });
            });

            $('#dt-table').on('click', '.hapus', function(e) {
                e.preventDefault();
                let id = $(this).data('id');
                Swal.fire({
                    title: 'Anda Yakin Ingin Menghapus Data Ini?',
                    showDenyButton: true,
                    showCancelButton: false,
                    confirmButtonText: 'Hapus',
                    denyButtonText: `Jangan Hapus`,
                }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "POST",
                            url: "data-kegiatan/delete",
                            data: {
                                id: id
                            },
                            dataType: "JSON",
                            success: function(response) {
                                LoadTableDatas();
                                if (response.success) {
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: 'Data Berhasil Di Hapus',
                                        showConfirmButton: false,
                                        timer: 1500
                                    })
                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: response.message,

                                    })
                                }

                            }
                        });
                    }
                })

            })

            $('form#form-kegiatan').submit(function(e) {
                e.preventDefault();

                $.ajax({
                    type: "post",
                    url: base_url + "data-kegiatan/save",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(response) {
                        $('#add-modal').modal('hide');
                        LoadTableDatas();
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: response.message,
                            showConfirmButton: false,
                            timer: 1500
                        })
                    }
                });
            });
        });
    </script>
@endsection
