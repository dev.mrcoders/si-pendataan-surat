<?= form_open_multipart('', ['class' => 'form-horizontal r-separator', 'id' => 'form-data-edit']) ?>
<div class="modal-body " id="checked-data">
	<input type="hidden" name="id_" class="id_" value="0">
    {{ FormBuilder(['type' => 'text', 'label' => 'Judul Surat', 'name' => 'judul_surat', 'class' => 'judul_surat_edit', 'id' => 'judul_surat_edit']) }}
    {{ FormBuilder(['type' => 'select', 'label' => 'Type Surat', 'name' => 'type_surat', 'class' => 'type_surat_edit', 'id' => 'type_surat_edit', 'vlabel' => ['MoU', 'MoA']]) }}
    {{ FormBuilder(['type' => 'select', 'label' => 'Jenis Surat', 'name' => 'jenis_surat', 'class' => 'jenis_surat_edit', 'id' => 'jenis_surat_edit']) }}
    {{ FormBuilder(['type' => 'text', 'label' => 'Nama Mitra', 'name' => 'mitra', 'class' => 'mitra_edit', 'id' => 'mitra_edit']) }}
    {{ FormBuilder(['type' => 'text', 'label' => 'No Pihak Pertama', 'name' => 'no_pihak_pertama', 'class' => 'no_pihak_pertama_edit', 'id' => 'no_pihak_pertamat_edit']) }}
    {{ FormBuilder(['type' => 'text', 'label' => 'No Pihak Kedua', 'name' => 'no_pihak_kedua', 'class' => 'no_pihak_kedua_edit', 'id' => 'no_pihak_kedua']) }}
    {{ FormBuilder(['type' => 'text', 'label' => 'Tanggal Mulai', 'name' => 'tgl_mulai', 'class' => 'tgl_mulai_edit', 'id' => 'tgl_mulai']) }}
    {{ FormBuilder(['type' => 'text', 'label' => 'Tanggal Berakhir', 'name' => 'tgl_berakhir', 'class' => 'tgl_berakhir_edit', 'id' => 'tgl_berakhir']) }}
	<div class="form-group row align-items-center m-b-0">
		<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Kegiatan Perjanjian</label>
        <div class="col-9 border-left p-b-10 p-t-10">
			<select name="kegiatan_perjanjian[]" id="kegiatan_perjanjian_edit" class="form-control kegiatan_perjanjian_edit" multiple></select>
		</div>
    </div>
	{{-- {{ FormBuilder(['type' => 'text', 'label' => 'Kegiatan Perjanjian', 'name' => 'kegiatan_perjanjian', 'class' => 'kegiatan_perjanjian_edit', 'id' => 'kegiatan_perjanjian']) }} --}}
    {{ FormBuilder(['type' => 'file', 'label' => 'File Surat', 'name' => 'file_surat', 'class' => 'file_surat_edit', 'id' => 'file_surat', 'accept' => 'application/pdf,.doc,application/vnd.openxmlformats-officedocument.wordprocessingml.document']) }}
    <div class="form-group row align-items-center m-b-0" hidden>
        <label for="inputEmail3" class="col-3 text-right control-label col-form-label">Gambar
            Kegiatan</label>
        <div class="col-9 border-left p-b-10 p-t-10">
            <div id="education_fields"></div>
            <div class="row ">
                <div class="col-sm-10">
                    <input type="file" class="form-control" id="Age" name="images[]">
                    <input type="hidden" name="jumlah[]" value="1" />
                </div>
                <div class="col-sm-2">
                    <button class="btn btn-success" type="button" onclick="education_fields();"><i
                            class="fa fa-plus"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-success waves-effect text-left">Simpan</button>
</div>
</form>
