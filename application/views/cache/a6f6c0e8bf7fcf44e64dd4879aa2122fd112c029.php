<?= form_open_multipart('', ['class' => 'form-horizontal r-separator', 'id' => 'form-data']) ?>
<div class="modal-body " id="checked-data">
    <input type="hidden" name="id_" value="0">
    <?php echo e(FormBuilder(['type' => 'text', 'label' => 'Judul Surat', 'name' => 'judul_surat', 'class' => 'judul_surat', 'id' => 'judul_surat'])); ?>

    <?php echo e(FormBuilder(['type' => 'select', 'label' => 'Type Surat', 'name' => 'type_surat', 'class' => 'type_surat', 'id' => 'type_surat', 'vlabel' => ['MoU', 'MoA']])); ?>

    <?php echo e(FormBuilder(['type' => 'select', 'label' => 'Jenis Surat', 'name' => 'jenis_surat', 'class' => 'jenis_surat', 'id' => 'jenis_surat'])); ?>

    <?php echo e(FormBuilder(['type' => 'text', 'label' => 'Nama Mitra', 'name' => 'mitra', 'class' => 'mitra', 'id' => 'mitra'])); ?>

    <?php echo e(FormBuilder(['type' => 'text', 'label' => 'No Pihak Pertama', 'name' => 'no_pihak_pertama', 'class' => 'no_pihak_pertama', 'id' => 'no_pihak_pertamat'])); ?>

    <?php echo e(FormBuilder(['type' => 'text', 'label' => 'No Pihak Kedua', 'name' => 'no_pihak_kedua', 'class' => 'no_pihak_kedua', 'id' => 'no_pihak_kedua'])); ?>

    <?php echo e(FormBuilder(['type' => 'text', 'label' => 'Tanggal Mulai', 'name' => 'tgl_mulai', 'class' => 'tgl_mulai pickadate', 'id' => 'tgl_mulai'])); ?>

    <?php echo e(FormBuilder(['type' => 'text', 'label' => 'Tanggal Berakhir', 'name' => 'tgl_berakhir', 'class' => 'tgl_berakhir', 'id' => 'tgl_berakhir'])); ?>

    <div class="form-group row align-items-center m-b-0">
		<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Kegiatan Perjanjian</label>
        <div class="col-9 border-left p-b-10 p-t-10">
			<select name="kegiatan_perjanjian[]" id="kegiatan_perjanjian" class="form-control kegiatan_perjanjian" multiple></select>
		</div>
    </div>
    
    
    <?php echo e(FormBuilder(['type' => 'file', 'label' => 'File Surat', 'name' => 'file_surat', 'class' => 'file_surat', 'id' => 'file_surat', 'accept' => 'application/pdf,.doc,application/vnd.openxmlformats-officedocument.wordprocessingml.document'])); ?>

    <div class="form-group row align-items-center m-b-0">
        <label for="inputEmail3" class="col-3 text-right control-label col-form-label">Gambar
            Kegiatan</label>
        <div class="col-9 border-left p-b-10 p-t-10">
            <div id="education_fields"></div>
            <div class="row ">
                <div class="col-sm-10">
                    <input type="file" class="form-control" id="Age" name="images[]">
                    <input type="hidden" name="jumlah[]" value="1" />
                </div>
                <div class="col-sm-2">
                    <button class="btn btn-success" type="button" onclick="education_fields();"><i
                            class="fa fa-plus"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-success waves-effect text-left">Simpan</button>
</div>
</form>
<?php /**PATH D:\vhost\humas.poltek-kampar.ac.id\public_html\application\views/halaman/form/c_kerjasama.blade.php ENDPATH**/ ?>