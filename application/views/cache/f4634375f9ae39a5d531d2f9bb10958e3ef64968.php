<?= form_open('', ['id' => 'form-user-edit']) ?>
<input type="hidden" name="id_" id="id_">
<div class="modal-body">
    <?php echo e(FormBuilder(['type' => 'text', 'label' => 'NRP', 'name' => 'nrp', 'class' => 'nrp', 'id' => 'enrp'])); ?>

    <?php echo e(FormBuilder(['type' => 'text', 'label' => 'Firstname', 'name' => 'firstname', 'class' => 'firstname', 'id' => 'efirstname'])); ?>

    <?php echo e(FormBuilder(['type' => 'text', 'label' => 'Lastname', 'name' => 'lastname', 'class' => 'lastname', 'id' => 'elastname'])); ?>

    <?php echo e(FormBuilder(['type' => 'select', 'label' => 'Jabatan', 'name' => 'jabatan', 'class' => 'jabatan', 'id' => 'ejabatan'])); ?>

    <?php echo e(FormBuilder(['type' => 'text', 'label' => 'Username', 'name' => 'username', 'class' => 'username', 'id' => 'eusername'])); ?>

    <?php echo e(FormBuilder(['type' => 'select', 'label' => 'Role', 'name' => 'role', 'class' => 'role', 'id' => 'erole'])); ?>

    <?php echo e(FormBuilder(['type' => 'password', 'label' => 'Password', 'name' => 'password', 'class' => 'password', 'id' => 'epassword'])); ?>

    <div class="form-group row align-items-center m-b-0">
        <label for="inputEmail3" class="col-3 text-right control-label col-form-label"></label>
        <div class="col-9 border-left p-b-10 p-t-10">
            <div class="custom-control custom-checkbox mr-sm-2 m-b-15 col-sm-9">
                <input type="checkbox" class="custom-control-input" id="checkbox1" value="">
                <label class="custom-control-label" for="checkbox1">Lihat Password</label>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-success waves-effect text-left">Update</button>
</div>
</form>
<?php /**PATH D:\vhost\humas.poltek-kampar.ac.id\public_html\application\views/halaman/form/e_formuser.blade.php ENDPATH**/ ?>