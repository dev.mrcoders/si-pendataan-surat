<?php $__env->startSection('content'); ?>
    <style>
        tbody tr td {
            font-size: 11px;
        }
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4 class="card-title">Data Pengguna</h4>
                            </div>
                            <div class="col-sm-6 text-right">
                                <button class="btn btn-sm btn-primary tambah">Tambah Data</button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table" id="dt-table">
                                <thead class="bg-primary text-white">
                                    <tr>
                                        <th>#</th>
                                        <th>NRP</th>
                                        <th>Username</th>
                                        <th>Firtsname</th>
                                        <th>lastname</th>
                                        <th>Role</th>
                                        <th>Jabatan</th>
                                        <th>Act</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>SMK 001</td>
                                        <td>Nota Kesepahaman</td>
                                        <td>Kerjasam Bidang Pendidikan</td>
                                        <td>SMK</td>
                                        <td>MoU</td>
                                        <td>Pihak Pertama : 00000001<br>Pihak Kedua : 0000001</td>
                                        <td>
                                            <div class="btn-group">
                                                <button class="btn btn-sm btn-warning">Ubah</button>
                                                <button class="btn btn-sm btn-danger">Hapus</button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-example-modal-lg" id="modal-add-user" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Tambah Pengguna</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <?php echo $__env->make('halaman.form.c_formuser', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>

    <div class="modal fade bs-example-modal-lg" id="modal-edit-user" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Tambah Pengguna</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <?php echo $__env->make('halaman.form.e_formuser', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>

    <script>
        $(function() {
            LoadTableDatas();
            SelectRole();

            function SelectRole() {
                let html = '';
                html += '<option value="">== Pilih Role==</option>';
                $.ajax({
                    type: "GET",
                    url: base_url + "data-pengguna/list-role",
                    dataType: "JSON",
                    success: function(response) {
                        $.each(response, function(index, value) {
                            html += '<option value="' + value.id_role + '">' + value.nama_role +
                                '</option>';
                        });
                        $('#role, #erole').html(html);
                    }
                });
            }
            SelectJabatan();

            function SelectJabatan() {
                let html = '';
                html += '<option value="">== Pilih Jabatan==</option>';
                $.ajax({
                    type: "GET",
                    url: base_url + "data-pengguna/list-jabatan",
                    dataType: "JSON",
                    success: function(response) {
                        $.each(response, function(index, value) {
                            html += '<option value="' + value.id_jabatan + '">' + value
                                .nama_jabatan + '</option>';
                        });
                        $('#jabatan, #ejabatan').html(html);
                    }
                });
            }

            function LoadTableDatas() {
                $("#dt-table").DataTable({
                    stateSave: true, //dapat kembali sesuai page terakhir di klik
                    destroy: true,
                    ajax: {
                        url: base_url + "data-pengguna/tables-pengguna",
                        type: "POST",
                        dataSrc: "",
                    },
                    columns: [{
                            render: function(data, type, row, meta) {
                                return meta.row + 1;
                            },
                        },
                        {
                            data: "nrp"
                        },
                        {
                            data: "username"
                        },
                        {
                            data: "nama",
                            render: function(data) {
                                let firstname = data.split(' ');
                                return firstname[0];
                            }
                        },
                        {
                            data: "nama",
                            render: function(data) {
                                let lastname = data.split(' ');
                                return lastname[1];
                            }
                        },
                        {
                            data: "nama_role"
                        },
                        {
                            data: "nama_jabatan"
                        }
                    ],
                    columnDefs: [{
                        targets: 7,
                        data: "id_user",
                        render: function(data, type, row, meta) {
                            return (
                                '<div class="btn-group">' +
                                '<button class="btn btn-sm btn-warning edit" data-id="' +
                                data + '">Ubah</button>' +
                                '<button class="btn btn-sm btn-danger hapus" data-id="' +
                                data + '">Hapus</button>' +
                                '</div>'
                            );
                        },
                    }, ],
                });
            }

            $('#checkbox0').on('click', function() {
                let pass = $('#password');
                if (pass.attr('type') == 'password') {
                    pass.attr('type', 'text');
                } else {
                    pass.attr('type', 'password');
                }
            });

            $('.tambah').on('click', function() {
                $('#form-user')[0].reset();
                $('#modal-add-user').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });

            $('#dt-table').on('click', '.edit', function() {
                let id = $(this).data('id');
                $.ajax({
                    type: "GET",
                    url: base_url + "data-pengguna/tables-pengguna",
                    data: {
                        id: id
                    },
                    dataType: "JSON",
                    success: function(response) {
                        $('#id_').val(id);
                        $('#modal-edit-user').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('#enrp').val(response.nrp);
                        $('#efirstname').val(response.firstname);
                        $('#elastname').val(response.lastname);
                        $('#ejabatan').val(response.id_jabatan).trigger('change');
                        $('#eusername').val(response.username);
                        $('#erole').val(response.id_role).trigger('change');
                    }
                });
            })

            $('#dt-table').on('click', '.hapus', function(e) {
                e.preventDefault();
                let id = $(this).data('id');
                Swal.fire({
                    title: 'Anda Yakin Ingin Menghapus User Ini?',
                    showDenyButton: true,
                    showCancelButton: false,
                    confirmButtonText: 'Hapus',
                    denyButtonText: `Jangan Hapus`,
                }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "POST",
                            url: "data-pengguna/delete",
                            data: {
                                id: id
                            },
                            dataType: "JSON",
                            success: function(response) {
                                LoadTableDatas();
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'User Berhasil Di Hapus',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }
                        });
                    }
                })

            })

            $("#form-user").validate({
                rules: {
                    "nrp": "required",
                    "firstname": "required",
                    "lastname": "required",
                    "jabatan": "required",
                    "username": "required",
                    "role": "required",
                    "password": "required",

                },
                messages: {
                    "nrp": "Tidak Boleh Kosong",
                    "firstname": "Tidak Boleh Kosong",
                    "lastname": "Tidak Boleh Kosong",
                    "jabatan": "Tidak Boleh Kosong",
                    "username": "Tidak Boleh Kosong",
                    "role": "Tidak Boleh Kosong",
                    "password": "Tidak Boleh Kosong",

                },
                errorElement: "em",
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");
                    if (element.hasClass("select2")) {
                        error.insertAfter(element.next("span"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                submitHandler: SubmitUser,
            });

            $("#form-user-edit").validate({
                rules: {
                    "nrp": "required",
                    "firstname": "required",
                    "lastname": "required",
                    "jabatan": "required",
                    "username": "required",
                    "role": "required",
                    "password": false,

                },
                messages: {
                    "nrp": "Tidak Boleh Kosong",
                    "firstname": "Tidak Boleh Kosong",
                    "lastname": "Tidak Boleh Kosong",
                    "jabatan": "Tidak Boleh Kosong",
                    "username": "Tidak Boleh Kosong",
                    "role": "Tidak Boleh Kosong",


                },
                errorElement: "em",
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");
                    if (element.hasClass("select2")) {
                        error.insertAfter(element.next("span"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                submitHandler: SubmitUserEdit,
            });

            function SubmitUser() {
                let FormData = $('#form-user').serialize();
                $.ajax({
                    type: "POST",
                    url: base_url + "data-pengguna/save",
                    data: FormData,
                    dataType: "JSON",
                    success: function(response) {
                        LoadTableDatas();
                        $('#modal-add-user').modal('hide');
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Data Pengguna Berhasil Dibuat',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    }
                });
            }

            function SubmitUserEdit() {
                let FormData = $('#form-user-edit').serialize();
                $.ajax({
                    type: "POST",
                    url: base_url + "data-pengguna/save",
                    data: FormData,
                    dataType: "JSON",
                    success: function(response) {
                        LoadTableDatas();
                        $('#modal-edit-user').modal('hide');
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Data Pengguna Berhasil Diupdate',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    }
                });
            }
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\vhost\humas.poltek-kampar.ac.id\public_html\application\views/halaman/pengguna.blade.php ENDPATH**/ ?>