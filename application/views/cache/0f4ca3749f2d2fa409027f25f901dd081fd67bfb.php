<?= form_open('', ['id' => 'form-user']) ?>
<div class="modal-body">
    <?php echo e(FormBuilder(['type' => 'text', 'label' => 'NRP', 'name' => 'nrp', 'class' => 'nrp', 'id' => 'nrp'])); ?>

    <?php echo e(FormBuilder(['type' => 'text', 'label' => 'Firstname', 'name' => 'firstname', 'class' => 'firstname', 'id' => 'firstname'])); ?>

    <?php echo e(FormBuilder(['type' => 'text', 'label' => 'Lastname', 'name' => 'lastname', 'class' => 'lastname', 'id' => 'lastname'])); ?>

    <?php echo e(FormBuilder(['type' => 'select', 'label' => 'Jabatan', 'name' => 'jabatan', 'class' => 'jabatan', 'id' => 'jabatan'])); ?>

    <?php echo e(FormBuilder(['type' => 'text', 'label' => 'Username', 'name' => 'username', 'class' => 'username', 'id' => 'username'])); ?>

    <?php echo e(FormBuilder(['type' => 'select', 'label' => 'Role', 'name' => 'role', 'class' => 'role', 'id' => 'role'])); ?>

    <?php echo e(FormBuilder(['type' => 'password', 'label' => 'Password', 'name' => 'password', 'class' => 'password', 'id' => 'password'])); ?>

    <div class="form-group row align-items-center m-b-0">
        <label for="inputEmail3" class="col-3 text-right control-label col-form-label"></label>
        <div class="col-9 border-left p-b-10 p-t-10">
            <div class="custom-control custom-checkbox mr-sm-2 m-b-15 col-sm-9">
                <input type="checkbox" class="custom-control-input" id="checkbox0" value="">
                <label class="custom-control-label" for="checkbox0">Lihat Password</label>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-success waves-effect text-left">Simpan</button>
</div>
</form>
<?php /**PATH D:\vhost\humas.poltek-kampar.ac.id\public_html\application\views/halaman/form/c_formuser.blade.php ENDPATH**/ ?>