<?php $__env->startSection('content'); ?>
    <style>
        thead tr th {
            font-size: 11px;
        }

        tbody tr td {
            font-size: 11px;
        }

        .modal {
            z-index: 1050;
        }

        .mfp-wrap {
            z-index: 99999;
        }

        @media (min-width: 992px) {
            .modal-lg-costum {
                max-width: 1080px;
            }
        }
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-12">
                                <h4 class="card-title">Data Surat Kerjasama MoA & MoU</h4>
                            </div>
                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label for="">Jenis Kerjasama</label>
                                        <select class="form-control custom-select" id="filter-jenis"
                                            data-placeholder="Choose a Category" tabindex="1">
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="">Jenis Surat</label>
                                        <select class="form-control custom-select" id="filter-type"
                                            data-placeholder="Choose a Category" tabindex="1">
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label for="">Jenis Kegiatan</label>
                                        <select class="form-control" id="filter-kegiatan"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2 text-right mt-4">
                                <button class="btn btn-sm btn-info btn-block filter-data mt-2">Filter</button>
                                <button class="btn btn-sm btn-dark btn-block reset-filter">Reset Filter</button>
                                <a href="<?php echo e(base_url('data-kegiatan')); ?>" class="btn btn-sm btn-warning btn-block ">Setting Jenis Kegiatan</a>
                                <button class="btn btn-sm btn-primary btn-block tambah-data">Tambah Data</button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table" id="dt-table">
                                <thead class="bg-primary text-white">
                                    <tr>
                                        <th>#</th>
                                        <th>Mitra</th>
                                        <th>Judul Surat</th>
                                        <th>Kegiatan</th>
                                        <th>Jenis Surat</th>
                                        <th>Jenis Kerjasama</th>
                                        <th>No Surat</th>
                                        <th>Tanggal Surat</th>
                                        <th>File Berkas</th>
                                        <th>Act</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>SMK 001</td>
                                        <td>Nota Kesepahaman</td>
                                        <td>Kerjasam Bidang Pendidikan</td>
                                        <td>SMK</td>
                                        <td>MoU</td>
                                        <td>Pihak Pertama : 00000001<br>Pihak Kedua : 0000001</td>
                                        <td>Mulai : 12-12-2022<br>Berakhir: 12-12-2023</td>
                                        <td>MoU-smk.pdf</td>
                                        <td>
                                            <div class="btn-group">
                                                <button class="btn btn-sm btn-warning">Ubah</button>
                                                <button class="btn btn-sm btn-danger">Hapus</button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-lg" id="modal-data" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Form Buat Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <?php echo $__env->make('halaman.form.c_kerjasama', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade bs-example-modal-lg" id="modal-data-edit" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Form Buat Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <?php echo $__env->make('halaman.form.e_kerjasama', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade bs-example-modal-lg" id="modal-view-doc" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg-costum modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Priview Document</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div id="word-container" class="" hidden>
                    </div>
                    <div class="mx-auto">
                        <iframe id="priview-pdf" style="border:1px solid #666CCC" title="PDF in an i-Frame" src=""
                            frameborder="1" scrolling="auto" height="1100" width="100%" hidden></iframe>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>

    <div class="modal fade bs-example-modal-lg" id="modal-view-galery" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg-costum modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Priview Document</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <?php echo $__env->make('halaman.galery.list', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left"
                        data-dismiss="modal">Close</button>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
    <script>
        let role = "<?php echo e(userdata('role')); ?>";
        // console.log(role);
        $(function() {
            let formValidate;
            let table;
            LoadTableDatas();

            function LoadTableDatas() {
                $(".table-responsive").css("display", "block");
                table = $("#dt-table").DataTable({
                    stateSave: true, //dapat kembali sesuai page terakhir di klik
                    destroy: true,
                    ajax: {
                        url: base_url + "data-kerjasama/tables",
                        type: "POST",
                        data: function(data) {
                            data.is_delete = 'N';
                            data.jenis_kerjasama = $('#filter-jenis').val();
                            data.jenis_surat = $('#filter-type').val();
                            data.jenis_kegiatan = $('#filter-kegiatan').val();
                        },
                        dataSrc: "",
                    },
                    columns: [{
                            render: function(data, type, row, meta) {
                                return meta.row + 1;
                            },
                        },
                        {
                            data: "mitra"
                        },
                        {
                            data: "judul_surat"
                        },
                        {
                            data: "id_repo",
                            render: function(data) {
                                return kegitanList(data)
                            }
                        },
                        {
                            data: "nama_jenis"
                        },
                        {
                            data: {
                                type_repo: "type_repo"
                            },
                            render: function(d) {
                                return (d.type_repo == 1 ? 'MoA' : 'Mou');
                            }
                        },
                        {
                            data: {
                                no_pihak_pertama: "no_pihak_pertama",
                                no_pihak_kedua: "no_pihak_kedua",
                            },
                            render: function(d) {
                                return 'Pihak Pertama : ' + d.no_pihak_pertama +
                                    '<br>Pihak Kedua : ' + d.no_pihak_kedua;
                            }
                        },
                        {
                            data: {
                                tgl_mulai: "tgl_mulai",
                                tgl_berakhir: "tgl_berakhir",
                            },
                            render: function(d) {
                                return 'Mulai : ' + d.tgl_mulai +
                                    '<br>Berakhir : ' + d.tgl_berakhir;
                            }
                        },
                        {
                            data: {
                                file_surat: "file_surat"
                            },
                            render: function(d) {
                                return d.file_surat +
                                    '<br><div class="btn-group btn-group-sm" role="group" aria-label="Basic example">' +
                                    '<button type="button" class="btn btn-info priview" data-file="' +
                                    d.file_surat + '">Lihat File</button>' +
                                    '<button type="button" class="btn btn-secondary view-galery" data-galery="' +
                                    d.id_repo + '">Lihat Galery</button>' +
                                    '</div>';
                            }
                        }

                    ],

                    columnDefs: [{
                        targets: 9,
                        data: "id_repo",
                        render: function(data, type, row, meta) {
                            return (
                                '<div class="btn-group">' +
                                '<button class="btn btn-sm btn-warning edit" data-id="' +
                                data + '">Ubah</button>' +
                                '<button class="btn btn-sm btn-danger hapus" data-id="' +
                                data + '">Hapus</button>' +
                                '</div>'
                            );
                        },
                    }, ],
                });
            }

            function kegitanList(id) {
                let list = '';
                $.ajax({
                    type: "get",
                    url: base_url + "list-kegiatan",
                    data: {
                        id: id
                    },
                    dataType: "json",
                    async: false,
                    success: function(response) {
                        $.each(response, function(indexInArray, valueOfElement) {
                            list += '<span class="badge badge-pill badge-info">' +
                                valueOfElement.nama_kegiatan + '</span>';
                        });

                    }
                });
                return list;
            }

            if (role != 1) {
                $('#dt-table').css('width', '100%');
                table.columns(9).visible(false);
            }


            $('#tgl_mulai, #tgl_mulai_edit').bootstrapMaterialDatePicker({
                weekStart: 0,
                time: false
            });
            $('#tgl_berakhir, #tgl_berakhir_edit').bootstrapMaterialDatePicker({
                weekStart: 0,
                time: false
            });
            $('#filter-kegiatan').select2({
                width: '100%',
                theme: 'bootstrap4',
                allowClear: true,
                placeholder: 'masukkan nama kegiatan',
                ajax: {
                    dataType: 'json',
                    url: base_url + 'data-kegiatan/tables',
                    type: 'GET',
                    data: function(params) {
                        return {
                            search: params.term
                        }
                    },
                    processResults: function(data, page) {
                        const list = [];
                        $.each(data, function(ind, val) {
                            list.push({
                                'id': val.id_kegiatan,
                                'text': val.nama_kegiatan
                            });
                        });
                        return {
                            results: list
                        };
                    },
                }
            });

            $('#kegiatan_perjanjian,#kegiatan_perjanjian_edit').select2({
                width: '100%',
                theme: 'bootstrap4',
                allowClear: true,
                tags: true,
                tokenSeparators: [',', ' '],
                placeholder: 'masukkan nama kegiatan',
                ajax: {
                    dataType: 'json',
                    url: base_url + 'data-kegiatan/tables',
                    type: 'GET',
                    data: function(params) {
                        return {
                            search: params.term
                        }
                    },
                    processResults: function(data, page) {
                        const list = [];
                        $.each(data, function(ind, val) {
                            list.push({
                                'id': val.id_kegiatan,
                                'text': val.nama_kegiatan
                            });
                        });
                        return {
                            results: list
                        };
                    },
                }
            });

            $('.filter-data').on('click', function() {
                LoadTableDatas();
            });
            $('.reset-filter').on('click', function() {
                $('#filter-jenis, #filter-type').val('').trigger('change');
                $('#filter-kegiatan').val(null).trigger('change');
                LoadTableDatas();
            });
            $('.tambah-data').click(function(e) {
                e.preventDefault();
                $('#form-data')[0].reset();
                $('#form-data .invalid-feedback').remove()
                $('#form-data input, select').removeClass('is-valid');
                $('#form-data input, select').removeClass('is-invalid');
                $('#modal-data').modal({
                    backdrop: 'static',
                    keyboard: false
                });

            });

            $('#dt-table').on('click', '.edit', function(e) {
                e.preventDefault();
                let id = $(this).data('id');
				$('#kegiatan_perjanjian_edit').val(null).trigger('change');
                GetData(id);
                $('#modal-data-edit').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            })

            $('#dt-table').on('click', '.priview', function(e) {
                e.preventDefault();
                let file = $(this).data('file');
                let extension = file.split('.');
                if (extension[1] == 'docx') {
                    $('#word-container').removeAttr('hidden');
                    $('#priview-pdf').attr('hidden', true);
                    PreviewWordDoc(file);
                } else if (extension[1] == 'pdf') {
                    $('#priview-pdf').removeAttr('hidden');
                    $('#word-container').attr('hidden', true);
                    $('#modal-view-doc').modal('show');
                    $('#priview-pdf').attr('src', base_url + 'uploads/document/' + file);
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'File Tidak Bisa Di Priview, Browser tidak Mendukung extension ' +
                            extension[1],

                    })
                }

                // console.log(extension);
            })

            $('#dt-table').on('click', '.view-galery', function(e) {
                e.preventDefault();
                let id = $(this).data('galery');
                $('#modal-view-galery').modal('show');
                let html = '';
                $.ajax({
                    type: "GET",
                    url: base_url + "data-image",
                    data: {
                        id: id
                    },
                    dataType: "json",
                    success: function(response) {
                        $.each(response, function(index, value) {
                            html += '<div class="col-lg-3 col-md-6">' +
                                '<div class="card">' +
                                '<div class="el-card-item">' +
                                '<div class="el-card-avatar el-overlay-1"> <img src="<?php echo e(base_url('')); ?>uploads/images/' +
                                value.nama_file + '" alt="user">' +
                                '<div class="el-overlay">' +
                                '<ul class="list-style-none el-info">' +
                                '<li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="#" data-image="<?php echo e(base_url('')); ?>uploads/images/' +
                                value.nama_file +
                                '"><i class="icon-magnifier"></i></a></li>' +
                                '</ul>' +
                                '</div>' +
                                '</div>' +
                                '<div class="el-card-content">' +
                                '<h4 class="m-b-0">' + (value.type_repo == 1 ? 'MoU' :
                                    'MoA') + '  ' + value.tgl_mulai +
                                '</h4> <span class="text-muted">' + value.mitra +
                                '</span>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
                        });
                        $('#list-image').html(html);
                    }
                });
            });

            $('#list-image').on('click', '.image-popup-vertical-fit', function() {
                let image = $(this).data('image');
                $.magnificPopup.open({
                    items: {
                        src: image
                    },
                    type: 'image'
                });
            });

            $('#dt-table').on('click', '.hapus', function(e) {
                e.preventDefault();
                let id = $(this).data('id');
                Swal.fire({
                    title: 'Anda Yakin Ingin Menghapus Data Ini?',
                    showDenyButton: true,
                    showCancelButton: false,
                    confirmButtonText: 'Hapus',
                    denyButtonText: `Jangan Hapus`,
                }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "POST",
                            url: "data-kerjasama/delete",
                            data: {
                                id: id
                            },
                            dataType: "JSON",
                            success: function(response) {
                                LoadTableDatas();
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Data Berhasil Di Hapus',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }
                        });
                    }
                })

            })


            $.extend($.validator.prototype, {
                checkForm: function() {
                    this.prepareForm();
                    for (var i = 0, elements = (this.currentElements = this.elements()); elements[
                            i]; i++) {
                        if (this.findByName(elements[i].name).length != undefined && this.findByName(
                                elements[i].name).length > 1) {
                            for (var cnt = 0; cnt < this.findByName(elements[i].name).length; cnt++) {
                                this.check(this.findByName(elements[i].name)[cnt]);
                            }
                        } else {
                            this.check(elements[i]);
                        }
                    }
                    return this.valid();
                }
            });

            $.validator.addMethod('filesize', function(value, element, param) {
                return this.optional(element) || (element.files[0].size <= param * 1000000)
            }, 'File size must be less than {0} MB');

            $("#form-data").validate({
                rules: {
                    "judul_surat": "required",
                    "type_surat": "required",
                    "jenis_surat": "required",
                    "mitra": "required",
                    "no_pihak_pertama": "required",
                    "no_pihak_kedua": "required",
                    "tgl_mulai": "required",
                    "tgl_berakhir": "required",
                    "kegiatan_perjanjian": "required",
                    "file_surat": {
                        required: true,
                        accept: "application/pdf,.doc,application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                        extension: "pdf|docx|doc",
                        filesize: 5
                    },
                    "images[]": {
                        required: true,
                        accept: "image/jpg,image/jpeg",
                        extension: "jpeg|jpg",
                        filesize: 5
                    },

                },
                messages: {
                    "judul_surat": "Tidak Boleh Kosong",
                    "type_surat": "Pilih Salah Satu",
                    "jenis_surat": "Pilih Salah Satu",
                    "mitra": "Tidak Boleh Kosong",
                    "no_pihak_pertama": "Tidak Boleh Kosong",
                    "no_pihak_kedua": "Tidak Boleh Kosong",
                    "tgl_mulai": "Tidak Boleh Kosong",
                    "tgl_berakhir": "Tidak Boleh Kosong",
                    "kegiatan_perjanjian": "Tidak Boleh Kosomg",
                    "file_surat": {
                        required: "Wajib Di isi",
                        accept: "Pilih Sesuai format PDF,doc atau Docx",
                        extension: "pilih file yang sesuai format"
                    },
                    "images[]": {
                        required: "Wajib Di isi",
                        accept: "Pilih Sesuai format jpeg atau jpg",
                        extension: "pilih file yang sesuai format"
                    },
                },
                errorElement: "em",
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");
                    if (element.hasClass("select2")) {
                        error.insertAfter(element.next("span"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                submitHandler: SubmitKerjasama,
            });

            $("#form-data-edit").validate({
                rules: {
                    "judul_surat": "required",
                    "type_surat": "required",
                    "jenis_surat": "required",
                    "mitra": "required",
                    "no_pihak_pertama": "required",
                    "no_pihak_kedua": "required",
                    "tgl_mulai": "required",
                    "tgl_berakhir": "required",
                    "kegiatan_perjanjian": "required",

                },
                messages: {
                    "judul_surat": "Tidak Boleh Kosong",
                    "type_surat": "Pilih Salah Satu",
                    "jenis_surat": "Pilih Salah Satu",
                    "mitra": "Tidak Boleh Kosong",
                    "no_pihak_pertama": "Tidak Boleh Kosong",
                    "no_pihak_kedua": "Tidak Boleh Kosong",
                    "tgl_mulai": "Tidak Boleh Kosong",
                    "tgl_berakhir": "Tidak Boleh Kosong",
                    "kegiatan_perjanjian": "Tidak Boleh Kosomg",
                },
                errorElement: "em",
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");
                    if (element.hasClass("select2")) {
                        error.insertAfter(element.next("span"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                submitHandler: SubmitEditKerjasama,
            });

            function SubmitKerjasama() {
                let frm = $('#form-data')[0];
                let Data = new FormData(frm);
                $.ajax({
                    type: "POST",
                    url: "<?php echo e(base_url('data-kerjasama/save')); ?>",
                    data: Data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    async: false,
                    success: function(response) {
                        console.log(response);
                        if (response.success) {
                            $('#modal-data').modal('hide');
                            LoadTableDatas();
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: response.message,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        } else {
                            Swal.fire({
                                position: 'top-end',
                                icon: 'error',
                                title: response.message,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    },
                    error: function(request, textStatus, errorThrown) {
                        console.log(request.responseText);
                    }
                });
            }

            function SubmitEditKerjasama() {
                let frm = $('#form-data-edit')[0];
                let Data = new FormData(frm);
                $.ajax({
                    type: "POST",
                    url: "<?php echo e(base_url('data-kerjasama/save')); ?>",
                    data: Data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    async: false,
                    success: function(response) {
                        $('#form-data-edit')[0].reset();
						$('#kegiatan_perjanjian_edit').val(null).trigger('change');
                        $('#form-data-edit .invalid-feedback').remove()
                        $('#form-data-edit input, select').removeClass('is-valid');
                        $('#form-data-edit input, select').removeClass('is-invalid');
                        if (response.success) {
                            $('#modal-data-edit').modal('hide');
                            LoadTableDatas();
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: response.message,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        } else {
                            Swal.fire({
                                position: 'top-end',
                                icon: 'error',
                                title: response.message,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    },
                    error: function(request, textStatus, errorThrown) {
                        console.log(request.responseText);
                    }
                });
            }

        })

        function GetData(id) {
            $.ajax({
                type: "GET",
                url: base_url + "data-kerjasama/tables",
                data: {
                    id: id
                },
                dataType: "JSON",
                success: function(response) {
                    $('.id_').val(id);
                    $('.judul_surat_edit').val(response.judul_surat);
                    $('.type_surat_edit').val(response.type_repo).trigger('change');
                    $('.jenis_surat_edit').val(response.id_jenis_surat).trigger('change');
                    $('.mitra_edit').val(response.mitra);
                    $('.no_pihak_pertama_edit').val(response.no_pihak_pertama);
                    $('.no_pihak_kedua_edit').val(response.no_pihak_kedua);
                    $('.tgl_mulai_edit').val(response.tgl_mulai);
                    $('.tgl_berakhir_edit').val(response.tgl_berakhir);
                    $('.tgl_berakhir_edit').val(response.tgl_berakhir);
                    // $('#kegiatan_perjanjian_edit').val([1, 2]).trigger('change');


                    // $('.kegiatan_perjanjian_edit').val(response.kegiatan_perjanjian);
                    JnsKegiatan(id);
                }
            });
        }

        function JnsKegiatan(id) {
            let dt = [];
            $.ajax({
                type: "get",
                url: base_url + "list-kegiatan",
                data: {
                    id: id
                },
                dataType: "json",
                success: function(response) {
                    $.each(response, function(index, val) {
                        dt.push(new Option(val.nama_kegiatan, val.id_kegiatan, true, true));
                    });
                    $('#kegiatan_perjanjian_edit').append(dt).trigger('change');

                }
            });
        }

        SelectTypeSurat();

        function SelectTypeSurat() {
            const opsi = ['MoA', 'MoU'];
            let option = '';
            option += '<option value="">== Pilih Data ==</option>';
            $.each(opsi, function(index, value) {
                let vl = index + 1;
                option += '<option value="' + vl + '">' + value + '</option>';
            });

            $('#type_surat, #type_surat_edit, #filter-type').html(option);
        }
        SelectJenisSurat();

        function SelectJenisSurat() {
            let option = '';
            $.ajax({
                type: "GET",
                url: "<?php echo e(base_url()); ?>data-jenis",
                dataType: "JSON",
                success: function(response) {
                    $.each(response, function(index, value) {
                        option += '<option value="' + value.id + '">' + value.text + '</option>';
                    });
                    $('#jenis_surat, #jenis_surat_edit, #filter-jenis').html(option);
                }


            });
        }

        function PreviewWordDoc(file = null) {
            //URL of the Word Document.
            var url = base_url + "uploads/document/" + file;

            //Send a XmlHttpRequest to the URL.
            var request = new XMLHttpRequest();
            request.open('GET', url, true);
            request.responseType = 'blob';
            request.onload = function() {
                //Set the ContentType to docx.
                var contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";


                //Convert BLOB to File object.
                var doc = new File([request.response], contentType);

                //If Document not NULL, render it.
                if (doc != null) {
                    //Set the Document options.
                    var docxOptions = Object.assign(docx.defaultOptions, {
                        useMathMLPolyfill: true
                    });

                    $('#modal-view-doc').modal('show');
                    //Reference the Container DIV.
                    var container = document.querySelector("#word-container");

                    //Render the Word Document.
                    docx.renderAsync(doc, container, null, docxOptions);
                }

            };
            request.send();
        };


        var room = 1;

        function education_fields() {

            room++;
            var objTo = document.getElementById('education_fields')
            var divtest = document.createElement("div");
            divtest.setAttribute("class", "row mt-1 mb-1 removeclass" + room);
            var rdiv = 'removeclass' + room;
            divtest.innerHTML =
                '<div class="col-sm-10"><input type="file" class="form-control" id="Schoolname" name="images[]" ><input type="hidden" name="jumlah[]" value="' +
                room +
                '"/></div><div class="col-sm-2"> <button class="btn btn-danger" type="button" onclick="remove_education_fields(' +
                room + ');"> <i class="fa fa-minus"></i> </button> </div>';
            objTo.appendChild(divtest)
        }

        function remove_education_fields(rid) {
            $('.removeclass' + rid).remove();
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\vhost\humas.poltek-kampar.ac.id\public_html\application\views/halaman/kerjasama.blade.php ENDPATH**/ ?>