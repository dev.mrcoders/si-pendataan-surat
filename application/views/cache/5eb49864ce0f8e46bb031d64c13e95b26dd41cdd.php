<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(base_url('assets/')); ?>assets/images/favicon.png">
    <title>Nice admin Template - The Ultimate Multipurpose admin template</title>
    <!-- Custom CSS -->
    <link href="<?php echo e(base_url('assets/')); ?>assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="<?php echo e(base_url('assets/')); ?>assets/extra-libs/c3/c3.min.css" rel="stylesheet">
    <link href="<?php echo e(base_url('assets/')); ?>assets/extra-libs/jvector/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="<?php echo e(base_url('assets/')); ?>dist/css/style.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo e(base_url('assets/')); ?>assets/libs/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@x.x.x/dist/select2-bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo e(base_url('assets/')); ?>assets/libs/summernote/dist/summernote-bs4.css">
    
    
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.4.0/css/responsive.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.3.1/css/fixedHeader.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/rowgroup/1.3.0/css/rowGroup.dataTables.min.css">
    <link rel="stylesheet" href="<?php echo e(base_url()); ?>node_modules/@sweetalert2/theme-bulma/bulma.css">
    <link href="<?php echo e(base_url('assets/')); ?>assets/libs/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo e(base_url('assets/')); ?>assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
    <link rel="stylesheet" type="text/css"
        href="<?php echo e(base_url('assets/')); ?>assets/libs/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css">
    <script src="<?php echo e(base_url('assets/')); ?>assets/libs/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="https://unpkg.com/jszip/dist/jszip.min.js"></script>
    <script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
	<script src="<?php echo e(base_url('assets/')); ?>assets/libs/Chart.js/dist/Chart.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php echo $__env->make('layout.asside', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Dashboard</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <?php echo $__env->yieldContent('content'); ?>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- customizer Panel -->
    <!-- ============================================================== -->
    
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->

    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo e(base_url('assets/')); ?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo e(base_url('assets/')); ?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="<?php echo e(base_url('assets/')); ?>dist/js/app.min.js"></script>
    <script>
        const base_url = '<?php echo e(base_url('')); ?>';
        $(function() {
            $("#main-wrapper").AdminSettings({
                Theme: false, // this can be true or false ( true means dark and false means light ),
                Layout: 'vertical',
                LogoBg: 'skin2', // You can change the Value to be skin1/skin2/skin3/skin4/skin5/skin6 
                NavbarBg: 'skin2', // You can change the Value to be skin1/skin2/skin3/skin4/skin5/skin6
                SidebarType: 'full', // You can change it full / mini-sidebar / iconbar / overlay
                SidebarColor: 'skin2', // You can change the Value to be skin1/skin2/skin3/skin4/skin5/skin6
                SidebarPosition: true, // it can be true / false ( true means Fixed and false means absolute )
                HeaderPosition: true, // it can be true / false ( true means Fixed and false means absolute )
                BoxedLayout: false, // it can be true / false ( true means Boxed and false means Fluid ) 
            });
        });
    </script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo e(base_url('assets/')); ?>assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php echo e(base_url('assets/')); ?>assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo e(base_url('assets/')); ?>dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo e(base_url('assets/')); ?>dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo e(base_url('assets/')); ?>dist/js/custom.min.js"></script>
    <!--This page JavaScript -->
   
    <!--c3 charts -->
    <script src="<?php echo e(base_url('assets/')); ?>assets/extra-libs/c3/d3.min.js"></script>
    <script src="<?php echo e(base_url('assets/')); ?>assets/extra-libs/c3/c3.min.js"></script>
    <script src="<?php echo e(base_url('assets/')); ?>assets/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="<?php echo e(base_url('assets/')); ?>assets/extra-libs/jvector/jquery-jvectormap-world-mill-en.js"></script>
    

    <script src="<?php echo e(base_url('assets/')); ?>assets/libs/moment/moment.js"></script>
    <script
        src="<?php echo e(base_url('assets/')); ?>assets/libs/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker-custom.js">
    </script>
    
    <script src="<?php echo e(base_url('assets/')); ?>assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="<?php echo e(base_url('assets/')); ?>Scripts/docx-preview.js"></script>

    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script src="<?php echo e(base_url()); ?>node_modules/sweetalert2/dist/sweetalert2.min.js"></script>
    
    <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/rowgroup/1.3.0/js/dataTables.rowGroup.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.4.0/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.3.1/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo e(base_url('assets/')); ?>assets/libs/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo e(base_url('assets/')); ?>assets/libs/summernote/dist/summernote-bs4.min.js"></script>
	<script src="<?php echo e(base_url('assets/')); ?>assets/libs/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo e(base_url('assets/')); ?>assets/libs/select2/dist/js/select2.min.js"></script>
	<script src="<?php echo e(base_url('assets/')); ?>assets/libs/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
	


</body>

</html>
<?php /**PATH D:\vhost\humas.poltek-kampar.ac.id\public_html\application\views/template.blade.php ENDPATH**/ ?>