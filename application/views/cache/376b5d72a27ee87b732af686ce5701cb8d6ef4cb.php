<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    
                    <div class="card-img-overlaya">
                        <div class="row">
                            <div class="col-sm-12 p-2 text-white text-center bg-secondary">
                                <h2 class="font-weight-bold text-uppercase mb-0">Selamat datang di sistem repository Humas
                                    politeknik
                                    kampar</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="card border-bottom border-info">
                    <div class="card-body">
                        <div class="d-flex no-block align-items-center">
                            <div>
                                <h2 id="jml-kerjasama"></h2>
                                <h6 class="text-info">Total Surat Kerjasama</h6>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card border-bottom border-cyan">
                    <div class="card-body">
                        <div class="d-flex no-block align-items-center">
                            <div>
                                <h2 id="jml-mitra"></h2>
                                <h6 class="text-cyan">Total Mitra Kerjasama</h6>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card border-bottom border-success">
                    <div class="card-body">
                        <div class="d-flex no-block align-items-center">
                            <div>
                                <h2 id="jml-berakhir"></h2>
                                <h6 class="text-success">Total Kerjasama Kadaluarsa</h6>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card border-bottom border-orange">
                    <div class="card-body">
                        <div class="d-flex no-block align-items-center">
                            <div>
                                <h2 id="jml-aktif"></h2>
                                <h6 class="text-orange">Total Kerjasama Berlangsung</h6>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Chart Statistik Jenis Kerjasama</h4>
                        <div>
                            <div style="position: absolute; inset: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"
                                class="chartjs-size-monitor">
                                <div class="chartjs-size-monitor-expand"
                                    style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                    <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                                </div>
                                <div class="chartjs-size-monitor-shrink"
                                    style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                    <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                                </div>
                            </div>
                            <canvas id="bar-chart" style="display: block; width: 530px; height: 265px;"
                                class="chartjs-render-monitor" width="530" height="265"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Statistik Berdasarkan Jenis Kerjasama</h4>
                        <div class="row " id="list-statistik">



                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function() {
            let html = '';
            let labels = [];
			let datas = [];
            $.get(base_url + "dashboard/widget-app",
                function(data, textStatus, jqXHR) {

                    $('#jml-kerjasama').text(data.data.jmlkerjasama)
                    $('#jml-mitra').text(data.data.jmlmitra)
                    $('#jml-berakhir').text(data.data.jmlberakhir)
                    $('#jml-aktif').text(data.data.jmlaktif)

                    $.each(data.data.byjenis, function(index, val) {
                        labels.push(val.nama_jenis);
                        datas.push(val.jml);
                        html += '<div class="col-lg-12">' +
                            '<div class="card bg-info text-white">' +
                            '<div class="card-body p-2">' +
                            '<div class="d-flex no-block align-items-center">' +
                            '<div class="m-l-15 m-t-10">' +
                            '<h4 class="font-medium m-b-0">' + val.nama_jenis + '</h4>' +
                            '<h5>' + val.jml + '</h5>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>';
                    });
                    $('#list-statistik').html(html);
                    console.log(labels);

                    new Chart(document.getElementById("bar-chart"), {
                        type: 'bar',
                        data: {
                            labels: labels,
                            datasets: [{
                                label: "Population (millions)",
                                backgroundColor: ["#03a9f4", "#e861ff", "#08ccce", "#e2b35b",
                                    "#e40503"
                                ],
                                data: datas
                            }]
                        },
                        options: {
                            legend: {
                                display: false
                            },
                            
                        }
                    });
                },
                "json"
            );


        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\vhost\humas.poltek-kampar.ac.id\public_html\application\views/halaman/dashboard.blade.php ENDPATH**/ ?>