<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap">
                    <i class="mdi mdi-dots-horizontal"></i>
                    <span class="hide-menu">Personal</span>
                </li>
				
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo e(base_url('dashboard')); ?>"
                        aria-expanded="false">
                        <i class="mdi mdi-av-timer"></i>
                        <span class="hide-menu">Dashboard </span>
                        
                    </a>
                </li>
				<li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo e(base_url('data-kerjasama')); ?>"
                        aria-expanded="false">
                        <i class="mdi mdi-file-document-box"></i>
                        <span class="hide-menu">Data Kerjasama</span>
                        
                    </a>
                </li>
				<li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo e(base_url('report-kerjasama')); ?>"
                        aria-expanded="false">
                        <i class="mdi mdi-file-chart"></i>
                        <span class="hide-menu">Laporan Kerjasama</span>
                        
                    </a>
                </li>
				<?php if(userdata('role') == 1): ?>
				<li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo e(base_url('data-pengguna')); ?>"
                        aria-expanded="false">
                        <i class="mdi mdi-account-box"></i>
                        <span class="hide-menu">Data Pengguna</span>
                        
                    </a>
                </li>
				<?php endif; ?>
                
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo e(base_url('login/is-logout')); ?>"
                        aria-expanded="false">
                        <i class="mdi mdi-directions"></i>
                        <span class="hide-menu">Log Out</span>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<?php /**PATH D:\vhost\humas.poltek-kampar.ac.id\public_html\application\views/layout/asside.blade.php ENDPATH**/ ?>