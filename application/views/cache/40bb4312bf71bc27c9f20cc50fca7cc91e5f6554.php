<?php $__env->startSection('content'); ?>
    <style>
        tbody tr td {
            font-size: 11px;
        }
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <div class="row mb-2">
                            <div class="col-sm-12">
                                <h4 class="card-title">Data Laporan Kerjasama MoA & MoU</h4>
                            </div>
                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="form-group col-sm-6" hidden>
                                        <label for="">Jenis Kerjasama</label>
                                        <select class="form-control custom-select" id="filter-jenis"
                                            data-placeholder="Choose a Category" tabindex="1">
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6" hidden>
                                        <label for="">Jenis Surat</label>
                                        <select class="form-control custom-select" id="filter-type"
                                            data-placeholder="Choose a Category" tabindex="1">
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6" hidden>
                                        <label for="">Jenis Kegiatan</label>
                                        <select class="form-control" id="filter-kegiatan"></select>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label for="">Tahun</label>
                                        <select class="form-control" id="filter-tahun">
                                            <option value="">==Pilih Tahun==</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2 text-right mt-4">
                                <button class="btn btn-sm btn-info btn-block filter-data mt-2">Filter</button>
                                <button class="btn btn-sm btn-dark btn-block reset-filter">Reset Filter</button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table" id="dt-table">
                                <thead class="bg-primary text-white">
                                    <tr>
                                        <th>#</th>
                                        <th>Mitra</th>
                                        <th>Judul Surat</th>
                                        <th>Kegiatan</th>
                                        <th>Jenis Surat</th>
                                        <th>Jenis Kerjasama</th>
                                        <th>No Surat</th>
                                        <th>Tahun Kerjasama</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>SMK 001</td>
                                        <td>Nota Kesepahaman</td>
                                        <td>Kerjasam Bidang Pendidikan</td>
                                        <td>SMK</td>
                                        <td>MoU</td>
                                        <td>Pihak Pertama : 00000001<br>Pihak Kedua : 0000001</td>
                                        <td>Mulai : 12-12-2022<br>Berakhir: 12-12-2023</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        const years = [];
        const currentYear = (new Date()).getFullYear();
        $(function() {
            let formValidate;
            LoadTableDatas();

            function LoadTableDatas() {
                $("#dt-table").DataTable({
                    stateSave: true, //dapat kembali sesuai page terakhir di klik
                    destroy: true,
                    ajax: {
                        url: base_url + "data-kerjasama/tables",
                        type: "POST",
                        data: function(data) {
                            data.is_delete = '';
							data.jenis_kerjasama = $('#filter-jenis').val();
                            data.jenis_surat = $('#filter-type').val();
                            data.jenis_kegiatan = $('#filter-kegiatan').val();
                            data.tahun = $('#filter-tahun').val();
                        },
                        dataSrc: "",
                    },
                    columns: [{
                            render: function(data, type, row, meta) {
                                return meta.row + 1;
                            },
                        },
                        {
                            data: "mitra"
                        },
                        {
                            data: "judul_surat"
                        },
                        {
                            data: "id_repo",
                            render: function(data) {
                                return kegitanList(data)
                            }
                        },
                        {
                            data: "nama_jenis"
                        },
                        {
                            data: {
                                type_repo: "type_repo"
                            },
                            render: function(d) {
                                return (d.type_repo == 1 ? 'MoA' : 'Mou');
                            }
                        },
                        {
                            data: {
                                no_pihak_pertama: "no_pihak_pertama",
                                no_pihak_kedua: "no_pihak_kedua",
                            },
                            render: function(d) {
                                return 'Pihak Pertama : ' + d.no_pihak_pertama +
                                    '<br>Pihak Kedua : ' + d.no_pihak_kedua;
                            }
                        },
                        {
                            data: {
                                tgl_mulai: "tgl_mulai",
                                tgl_berakhir: "tgl_berakhir",
                            },
                            render: function(d) {
                                return 'Mulai : ' + d.tgl_mulai +
                                    '<br>Berakhir : ' + d.tgl_berakhir;
                            }
                        }
                    ],
                });
            }

            function kegitanList(id) {
                let list = '';
                $.ajax({
                    type: "get",
                    url: base_url + "list-kegiatan",
                    data: {
                        id: id
                    },
                    dataType: "json",
                    async: false,
                    success: function(response) {
                        $.each(response, function(indexInArray, valueOfElement) {
                            list += '<span class="badge badge-pill badge-info">' +
                                valueOfElement.nama_kegiatan + '</span>';
                        });

                    }
                });
                return list;
            }

            SelectTypeSurat();

            function SelectTypeSurat() {
                const opsi = ['MoA', 'MoU'];
                let option = '';
                option += '<option value="">== Pilih Data ==</option>';
                $.each(opsi, function(index, value) {
                    let vl = index + 1;
                    option += '<option value="' + vl + '">' + value + '</option>';
                });

                $('#type_surat, #type_surat_edit, #filter-type').html(option);
            }
            SelectJenisSurat();

            function SelectJenisSurat() {
                let option = '';
                $.ajax({
                    type: "GET",
                    url: "<?php echo e(base_url()); ?>data-jenis",
                    dataType: "JSON",
                    success: function(response) {
                        $.each(response, function(index, value) {
                            option += '<option value="' + value.id + '">' + value.text +
                                '</option>';
                        });
                        $('#jenis_surat, #jenis_surat_edit, #filter-jenis').html(option);
                    }


                });
            }

            $('#filter-kegiatan').select2({
                width: '100%',
                theme: 'bootstrap4',
                allowClear: true,
                placeholder: 'masukkan nama kegiatan',
                ajax: {
                    dataType: 'json',
                    url: base_url + 'data-kegiatan/tables',
                    type: 'GET',
                    data: function(params) {
                        return {
                            search: params.term
                        }
                    },
                    processResults: function(data, page) {
                        const list = [];
                        $.each(data, function(ind, val) {
                            list.push({
                                'id': val.id_kegiatan,
                                'text': val.nama_kegiatan
                            });
                        });
                        return {
                            results: list
                        };
                    },
                }
            });

            for (var i = currentYear; i >= 2015; i--) {
                years.push({
                    'id': i,
                    'text': i
                });
            }

            $('#filter-tahun').select2({
                width: '100%',
                theme: 'bootstrap4',
                allowClear: true,
                placeholder: '==Pilih tahun==',
                data: years
            });

            $('.filter-data').on('click', function() {
                LoadTableDatas();
            });
            $('.reset-filter').on('click', function() {
                $('#filter-jenis, #filter-type').val('').trigger('change');
                $('#filter-kegiatan, #filter-tahun').val(null).trigger('change');
                LoadTableDatas();
            });
        })
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\vhost\humas.poltek-kampar.ac.id\public_html\application\views/halaman/laporan.blade.php ENDPATH**/ ?>