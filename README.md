# SI-Pendataan Surat Politeknik Kampar

**Note: This project is still in progress, but welcome for any issues**

This repository is developed upon the following tools:

- [CodeIgniter](http://www.codeigniter.com/) (v3.1.13) - PHP framework
- [Bootstrap](http://getbootstrap.com/) (v4.1.x) - Popular frontend
- [BladeOne](https://github.com/EFTEC/BladeOne/) - feature-rich library engine blade view template
- [Route Statics](https://github.com/Patroklo/codeigniter-static-laravel-routes) - library route statics
- [NiceAdmin](https://www.wrappixel.com/templates/niceadmin/#demos) (v2.3.8) - bootstrap theme for Admin Panel


## Features

This repository contains setup for development
- Admin Panel withNiceAdmin Theme
- HMVC Konsep
- Blade View Like Laravel
- Route Statics Like Laravel
- Crud Data Kerjasama
- Crud Data Pengguna
- View laporan Kerjasama

## Server Environment

Below configuration are preferred; other environments are not well-tested, but still feel free to report and issues.

- **PHP 7.4+**
- **Apache 2.2+**
- **MySQL 5.7+**

## Setup Guide

1. git clone this repo
2. Create a database(example: named "humas-poltek"), then import /database/humas-poltek.sql into MySQL server
3. Make sure the database config (/application/config/database.php) is set correctly


## Admin default login accounts

- Admin login (default username: admin password: 123456)
- password use MD5 Encrypt

